#                             -*- Mode: Python -*- 
# util.py
# 

def analyze_seq (ref, seq, k):
    from Bio import pairwise2
    from Bio.SubsMat import MatrixInfo as matlist
    from Bio.pairwise2 import format_alignment
    
    m = 2
    n = 0
    matrix = {('A','A'): m, ('A','C'): n, ('A','G'): n, ('A','T'): n, ('A','N'): m, ('A','S'): m,
              ('C','A'): n, ('C','C'): m, ('C','G'): n, ('C','T'): n, ('C','N'): m, ('C','S'): m,
              ('G','A'): n, ('G','C'): n, ('G','G'): m, ('G','T'): n, ('G','N'): m, ('G','S'): m,
              ('T','A'): n, ('T','C'): n, ('T','G'): n, ('T','T'): m, ('T','N'): m, ('T','S'): m,
              ('N','A'): m, ('N','C'): m, ('N','G'): m, ('N','T'): m, ('N','N'): m, ('N','S'): m,
              ('S','A'): m, ('S','C'): m, ('S','G'): m, ('S','T'): m, ('S','N'): m, ('S','S'): m,
            }

    target=ref
    cleanseq = seq
    
    alignments = pairwise2.align.localds(target, cleanseq, matrix, -1, -0.1)
    print
    ref = alignments[0][0]
    itd = alignments[0][1]
    print ref
    print itd
    print

    print seq, len(cleanseq),

    if ref == itd:
        print "WT"
    elif len(cleanseq) < len(target):
        print "DELETION"
    elif len(cleanseq) == len(target):
        print "SNP",
        for i in xrange(0,len(cleanseq)):
            if target[i] == cleanseq[i]:
                pass
            else:
                print str(i+1)+cleanseq[i]+"/"+target[i],
        print

    else:
        left = [0,-1]
        middle = [-1, -1]
        right = [-1, len(ref)]
        for i in xrange(0,len(alignments[0][1])):
            if ref[i] == "-":
                if left[1] == -1:
                    left[1] = i
                    middle[0] = i
            else:
                if middle[0] != -1 and middle[1] == -1:
                    middle[1] = i
                    right[0] = i

        c1 = itd[left[0]:left[1]]
        c2 = itd[middle[0]:middle[1]]
        c3 = itd[right[0]:right[1]]
        
        br = len(c1) - (k - 1)
        dupl = len(c2) - (k - 1)

        print "ITD", 
        print c2, len(c2), len(cleanseq)-len(c2), middle[0],
        print


