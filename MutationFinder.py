#                             -*- Mode: Python -*- 
# MutationFinder.py
# 

import copy
import numpy as np
import os, string, sys
import logging as log
from Graph import Graph
from PathQuant import PathQuant
from Jellyfish import Jellyfish


class MutationFinder:

    def __init__ (self, ref_name, ref_seq, jf, graphical):
        ## Load the reference sequence and preparing ref k-mers

        first_seq = ref_seq[0:(jf.k)]
        last_seq = ref_seq[-(jf.k):]

        ref_mer = []
        for i in range (len (ref_seq) - (jf.k) + 1):
            kmer = ref_seq[i:(i + jf.k)]
            ref_mer.append (kmer)
        ref_set = set (ref_mer)
        log.debug ("Ref. set contains %d kmers.", len (ref_set))
        
        self.ref_set = ref_set
        self.ref_seq = ref_seq
        self.jf = jf
        self.node_data = {}
        self.done = set ()
        self.ref_mer = ref_mer
        self.ref_name = ref_name

        self.done.add (first_seq)
        self.node_data[first_seq] = self.jf.query (first_seq)
        self.done.add (last_seq)
        self.node_data[last_seq] = self.jf.query (last_seq)

        ## register all k-mers from the ref
        for s in self.ref_set:
            self.node_data[s] = self.jf.query (s)

        #self.genome_jf = Jellyfish("/ssd/home/gendrop/hg19")

        ## kmer walking from each k-mer of ref_seq
        self.done.update (self.ref_set)
        for seq in ref_set:
            if seq == last_seq: continue
            self.__extend ([seq], 0, 0)
    
        ## kmer walking from start only
        #self.__extend2 ([first_seq], 0, 0)

        #print len (self.node_data)

        self.graph_analysis (first_seq, last_seq, graphical)
        

    def __grow (self, max_dist): ## 150
        ## Building graphs with counts: Breadth first search

        for i in range (max_dist):  ## maximum distance from the ref
            if i == 0:
                stage = set ()
                for s in self.ref_set:
                    stage.add (s)
            next_stage = set ()
            for cur_seq in stage:
                if cur_seq in done:
                    continue

                child  = self.jf.get_child (cur_seq, forward=True)

                next_stage.update (child)
                done.add (cur_seq)
                node_data[cur_seq] = jf.query (cur_seq)

            stage = next_stage


    def __extend (self, stack, breaks, found):
        ## Recursive depth first search 
        #log.debug ("\033[KBranch length: %6d - Breaks: %3d - Paths: %3d\r" % (len (stack), breaks, found))
        #print >> sys.stderr, "Branch length: %6d - Breaks: %3d - Paths: %3d\r" % (len (stack), breaks, found),
        if len (stack) > 200:
            return
        cur_seq = stack[-1]
        child = self.jf.get_child (cur_seq, forward=True)
        
        if len (child) > 1:
            breaks += 1
            if breaks > 10:
                return
        
        for c in child:
            if c in self.done:
                
                self.done.update (stack)
                self.done.add (c)
                for p in stack:
                    self.node_data[p] = self.jf.query (p)
                self.node_data[cur_seq] = self.jf.query (cur_seq)
                found += 1
                #log.debug ("\033[KBranch length: %6d %3d\r" % (len (stack), found))
                #print >> sys.stderr, "Branch length: %6d %3d\r" % (len (stack), found),
            else:
                self.__extend (stack + [c], breaks, found)

    def __extend2 (self, stack, breaks, found):
        ## Depth first search 
        print >> sys.stderr, "Branch length: %6d - Breaks: %3d - Paths: %3d\r" % (len (stack), breaks, found),        
        if len (stack) == 1:
            self.done.update (self.ref_seq)
            for s in self.ref_set:
                self.node_data[s] = self.jf.query (s)    

        if len (stack) > (len (self.ref_seq) * 2):
            return
        cur_seq = stack[-1]
        child = self.jf.get_child (cur_seq, forward=True)

        if len (child) > 1:
            breaks += 1
            if breaks > 5:
                return                    
            
        for c in child:
            #if self.genome_jf.query(c) > 5:
            #    print "skipping", c, self.genome_jf.query(c)
            #    continue
            #print c, self.jf.query (c), self.genome_jf.query(c)

            if c in self.done:
                self.done.update (stack)
                self.done.add (c)
                for p in stack:
                    self.node_data[p] = self.jf.query (p)
                self.node_data[cur_seq] = self.jf.query (cur_seq)
                found += 1
                print >> sys.stderr, "Branch length: %6d %3d\r" % (len (stack), found),
                continue
            else:
                self.__extend2 (stack + [c], breaks, found)

    def graph_analysis (self, first_seq, last_seq, graphical):

        # log.debug ('\n')

        kmer = self.node_data.keys ()
        n = len (kmer)
        g = Graph (n)
        ref_i = map (lambda k: kmer.index (k), self.ref_mer)  # The reference path, with node numbers

        log.debug ("k-mer graph contains %d nodes.", n)

        for i in range (n):
            for j in range (n):
                if i == j:  continue
                if kmer[i][1:] == kmer[j][:-1]:
                    weight = 1
                    if kmer[i] in self.ref_seq and kmer[j] in self.ref_seq:  weight = 0.01
                    g[i, j] = weight
        
        g.init_paths (kmer.index (first_seq),
                      kmer.index (last_seq))

        z = g.all_shortest ()

        def get_seq (path, kmer, skip_prefix=True):
            path = list (path)
            if not path:  return ""  ## Deals with an empty sequence

            if skip_prefix:
                seq = kmer[path[0]][-1]
            else:
                seq = kmer[path[0]]

            for i in path[1:]:
                seq += kmer[i][-1]
            return seq

        def get_name (a, b, offset = 0):
            k = self.jf.k
            diff = g.diff_path (a, b, k)

            #print a
            #print b
            #print diff            

            de = diff[3][:-(k - 1)]
            ins = diff[4][:-(k - 1)]

            assert ((len(a)-len(de)+len(ins)) == len(b))

            if diff[0] == diff[1] and not diff[4]:
                return "Reference\t"
            else: 
                variant = "Indel"
                if diff[1] == diff[2]: # SNP have equal length specific sequences
                    variant = "Substitution"
                elif diff[0] == diff[5]: # ITD have zero kmers in ref after full trimming.  However, this does not distinguish cases where there is garbage between repeats.
                    variant = "ITD"    
                elif len (de) == 0 and len (ins) !=0:
                    variant = "Insertion"    
                elif len (de) != 0 and len (ins) ==0:
                    variant = "Deletion"    

                return "%s\t%d:%s:%d" % (variant,
                                         diff[0] + k + offset,
                                         (string.lower (get_seq (de, kmer, skip_prefix=True)) + "/" +
                                          get_seq (ins, kmer, skip_prefix=True)),
                                         diff[1] + 1 + offset)

        def get_counts (path, kmer, node_data):
            cnts = []
            for i in path:
                cnts += [self.node_data[kmer[i]]]
            return cnts

        ## Report and quantify the allele frequencies


        # Whole region version
        whole = False
        if whole:
            quant = PathQuant (all_path = z,
                               counts = self.node_data.values ())

            quant.compute_coef ()
            quant.refine_coef ()

            quant.get_ratio ()

            quant.output (db_f = self.jf.filename,
                          ref_name = self.ref_name,
                          name_f = lambda path: get_name (ref_i, path),
                          seq_f = lambda path: get_seq (path, kmer, skip_prefix=False))

            if graphical:
                import matplotlib.pyplot as plt

                plt.figure (figsize=(10, 6))
                for path, ratio in zip (z, quant.get_ratio ()):
                    plt.plot (get_counts (path, kmer, self.node_data) * ratio,
                              label=get_name (ref_i, path).split("\t")[0])
                plt.legend ()
                plt.show ()

            print "\n-- BETA"

        
        if len(z) == 1 and list(z[0]) == ref_i:
            quant = PathQuant (all_path = z,
                               counts = self.node_data.values ())
            
            quant.compute_coef ()
            quant.refine_coef ()
            
            quant.get_ratio ()
            
            if 0 in get_counts (z[0], kmer, self.node_data):
                quant.ratio = quant.coef * 0

            quant.output_beta (db_f = self.jf.filename,
                               ref_name = self.ref_name,
                               name_f = lambda path: get_name (ref_i, path),
                               seq_f = lambda path: get_seq (path, kmer, skip_prefix=False),
                               ref_path = z, info="cluster 0")
        else:
            variant_diffs = []
            variant_set = set(range(0, len(z)))
            for variant in z:
                diff = g.diff_path (ref_i, variant, self.jf.k)
                variant_diffs += [diff]

            def getIntersect(start, stop, v_set):
                for v in variant_set:
                    if variant_diffs[v][1] >= start and variant_diffs[v][0] <= stop:
                        return v
                return -1

            variant_groups = []
            while len(variant_set) > 0:
                seed = variant_set.pop ()
                grp = [seed]
                start = variant_diffs[seed][0]
                stop = variant_diffs[seed][1]
                v = getIntersect (start, stop, variant_set)
                while v != -1:
                    variant_set.remove(v)
                    grp += [v]
                    start = min(start, variant_diffs[v][0])
                    stop = max(stop, variant_diffs[v][1])
                    v = getIntersect (start, stop, variant_set)
                variant_groups += [(start, stop, grp)]

            cluster=0
            for gr in variant_groups:
                if len(gr[2]) == 1 and list(z[gr[2][0]]) == ref_i:
                    continue
                cluster+=1

                start = gr[0]
                stop = gr[1]
                #print start, stop
                var_size = max([abs(x[2]-x[1]+1) for x in [variant_diffs[v] for v in gr[2]]])
                offset = max(0, start - var_size)
                ref_path = ref_i[offset:stop]
                clipped_paths = [ ref_path ]
                for v in gr[2]:
                    #print variant_diffs[v]
                    start_off = offset
                    stop_off = variant_diffs[v][2] + (stop - variant_diffs[v][1])
                    clipped_paths += [ z[v][start_off:stop_off] ]

                # print offset
                # for c in clipped_paths:
                #     print c

                # for c in clipped_paths:
                #     print get_seq (c, kmer, skip_prefix=False)

                quant = PathQuant (all_path = clipped_paths,
                                   counts = self.node_data.values ())

                quant.compute_coef ()
                quant.refine_coef ()

                quant.get_ratio ()

                quant.output_beta (db_f = self.jf.filename,
                                   ref_name = self.ref_name,
                                   name_f = lambda path: get_name (ref_path, path, offset),
                                   seq_f = lambda path: get_seq (path, kmer, skip_prefix=False),
                                   ref_path = ref_path,
                                   info = "cluster %d n=%d" % (cluster,len(gr[2])))

                if graphical:
                    import matplotlib.pyplot as plt
                    
                    plt.figure (figsize=(10, 6))
                    for path, ratio in zip (clipped_paths, quant.get_ratio ()):
                        if path == ref_path:
                            plt.plot (get_counts (path, kmer, self.node_data) * ratio,
                                      label="Reference")
                        else:
                            plt.plot (get_counts (path, kmer, self.node_data) * ratio,
                                      label=get_name (ref_i, path).split("\t")[0])
                    plt.legend ()
                    plt.show ()



    @staticmethod
    def output_header ():
        PathQuant.output_header ()
