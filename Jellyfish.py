#                             -*- Mode: Python -*- 
# Jellyfish.py --- Provides a python front-end to query a Jellyfish database.
# 

import os, sys, math, glob, re
import subprocess
import logging as log

def prepare (infiles, outfile, k = 20, gz = True, max_ram = 8):
    ## max_ram in GB
    nb_threads = 4
    nb_generators = 2
    counter_len = 12 ## in bits
    s = int (0.50 * (8 * 1073741824 * max_ram) / (k + counter_len))
    print s
    if gz:
        cmd = "ls -C1 %s | xargs -n 1 echo gunzip -c > generators" % ' '.join (infiles)
        # print cmd
        os.system (cmd)
        in_part = "-G %d -g generators" % nb_generators
        # print in_part
    else:
        in_part = ' '.join (infiles)
    cmd = ("jellyfish count -m %d -o %s -c %d -s %d -t %d -C '-Q+' %s" % 
           (k, outfile, counter_len, s, nb_threads, in_part))
    #print cmd
    os.system (cmd)

class Jellyfish:
    
    def __init__ (self, filename, cutoff=0.30, n_cutoff=500):
        cmdline = subprocess.check_output (["jellyfish", "info", filename])
        m = re.search ('-m ([^ ]*)', cmdline)
        self.k = int (m.group (1))
        log.debug ("Opening %s, k = %d.", filename, self.k)
        
        self.jf = subprocess.Popen (["jellyfish", 
                                     "query", "-i", filename], 
                                    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=None)
        self.filename = filename
        self.cutoff = cutoff
        self.n_cutoff = n_cutoff

    def query (self, seq):
        self.jf.stdin.write (seq + '\n')
        res = self.jf.stdout.readline ()
        return int (res.strip ())

    def get_next (self, seq):
        prev = self.query (seq)
        max_count = -1
        for c in ['A', 'C', 'G', 'T']:
            count = self.query (seq[1:] + c) 
            #print c, ":", count
            if count > max_count:
                max_count = count
                next_c = c
        return (seq[1:] + next_c, max_count)

    def get_child (self, seq, forward=True):
        count = self.query (seq)
        child = []
        sum = 0
        for c in ['A', 'C', 'G', 'T']:
            if forward:  c_seq = seq[1:] + c
            else:  c_seq = c + seq[0:-1]
            c_count = self.query (c_seq)
            child.append ((c_seq, c_count))
            sum += c_count
        threshold = max (sum * self.cutoff, self.n_cutoff)

        return map (lambda x: x[0], filter (lambda x: x[1] > threshold, child))

    def terminate (self):
        self.jf.terminate ()
