#                             -*- Mode: Python -*- 
# PathQuant.py
# 

import numpy as np
import logging as log

class PathQuant:

    def __init__ (self, all_path, counts):
        self.all_path = all_path
        self.nb_kmer = len (counts)
        self.nb_seq = len (all_path)
        
        self.contrib = np.zeros ((self.nb_kmer, self.nb_seq), dtype=np.int32)
        self.counts = np.zeros ((self.nb_kmer, 1), dtype=np.float32)

        seq_i = 0
        log.debug ("%d sequence(s) are observed.", self.nb_seq)

        for s in all_path:
            for i in s:
                self.contrib[i, seq_i] += 1
            seq_i += 1

        self.counts[:,0] = counts


    def compute_coef (self):
        (coef, residual, rank, s) = np.linalg.lstsq (self.contrib, self.counts)
        self.coef = coef
        log.debug ("Linear fitting = %s", self.coef.flatten ())
    
    def refine_coef (self):
        ## applies a gradient descent to get rid of negative coefficients
        self.coef[self.coef < 0] = 0
        last_max_grad = np.inf
        iter = 0
        while last_max_grad > 0.01:  ## convergence threshold
            grad = np.zeros_like (self.coef, dtype=np.float32)
            counts_hat = np.dot (self.contrib, self.coef)
            for j in range (self.nb_seq):
                grad[j, 0] = np.sum (2 * (self.counts - counts_hat) *
                                     self.contrib[:,j].reshape (counts_hat.shape))
            grad /= self.nb_kmer
            self.coef += 0.1 * grad
            grad[self.coef < 0] = 0
            self.coef[self.coef < 0] = 0
            last_max_grad = np.max (np.abs (grad))
            iter += 1
            log.debug ("Iteration = %d, max_gradient = %f", iter, last_max_grad)
        log.debug ("Refined fitting = %s", self.coef.flatten ())

    def get_ratio (self):
        self.ratio = self.coef / np.sum (self.coef)
        return self.ratio
    
    @staticmethod
    def output_header ():
        print "Database\tQuery\tType\tVariant name\tRatio\tExpr\tSeq"
        
    def output (self, db_f, ref_name, name_f, seq_f):
        for i in range (self.nb_seq):
            #if self.ratio[i] > 0:
            print "%s\t%s\t%s\t%.3f\t%.1f\t%s" % (db_f,
                                                  ref_name, name_f (self.all_path[i]),
                                                  self.ratio[i], self.coef[i],
                                                  seq_f (self.all_path[i]))

    def output_beta (self, db_f, ref_name, name_f, seq_f, ref_path, info=""):
        ref_i = -1
        for i in range (self.nb_seq):
            if self.all_path[i] == ref_path:
                ref_i = i
        for i in range (self.nb_seq):
            if i != ref_i:
                print ("%s\t%s\t%s\t%.3f\t%.1f\t%s\t%.3f\t%.1f\t%s\t%s" %
                       (db_f,
                        ref_name, name_f (self.all_path[i]),
                        self.ratio[i], self.coef[i],
                        seq_f (self.all_path[i]),
                        self.ratio[ref_i], self.coef[ref_i],
                        seq_f (self.all_path[ref_i]), info))
                       
