#                             -*- Mode: Python -*- 
# MerGraph.py ---  a DeBruijn graph on a subset of k-mers
# 

from collections import defaultdict

class Node (object):
    
    def __init__ (self):
        self.seq = None
        self.count = {}
        self.is_ref = False
        self.is_marked = 0 # number of marks
    
    def __str__ (self):
        attr = ''
        if self.is_ref:
            attr += ',style=filled,peripheries=1,color="grey80"'
        return '%s [label="%s\\n%s"%s];' % (self.seq, self.seq, self.get_count (), attr)
    
    def get_count (self):
        groups = 8
        keys = self.count.keys ()
        return ',\\n'.join (map (lambda x: ", ".join (map (lambda p: "%s:%d" % (p, self.count[p]), keys[x:(x+groups)])), range ((len (keys) - 1) / groups + 1)))
        #return ',\\n'.join (map (lambda x: ", ".join (keys[x:(x+groups)]), range ((len (keys) - 1) / groups + 1)))

class MerGraph:
    
    def __init__ (self):
        self.nodes = {}
        
    def merge_nodes (self, nodes, p):
        for k in nodes:
            if k in self.nodes:
                node = self.nodes[k]
                node.count.update (nodes[k].count)
            else:
                node = nodes[k]
                self.nodes[k] = node

    def prep_edges (self):
        self.edges_start = defaultdict (list)
        self.edges_end = defaultdict (list)
        for k in self.nodes:
            node = self.nodes[k]
            self.edges_start[node.seq[1:]].append (node)
            self.edges_end[node.seq[0:-1]].append (node)

    def write_dot (self, out):
        self.prep_edges ()
        out.write ("digraph G {\n")
        for mid in self.edges_start:
            if mid in self.edges_end:
                for i in self.edges_start[mid]:
                    for j in self.edges_end[mid]:
                        if i.is_marked == 2 and j.is_marked == 2:
                            out.write ("  %s -> %s;\n" % (i.seq, j.seq))
        for i in self.nodes:
            if self.nodes[i].is_marked == 2:
                out.write ("  %s\n" % self.nodes[i])
        
        out.write ("}\n")
        
    def mark_fb (self, first, last):
        self.prep_edges ()
        # Backward from last
        stack = set ([last])
        while (stack):
            cur = stack.pop ()
            if self.nodes[cur].is_marked == 1:  continue
            self.nodes[cur].is_marked = 1
            stack.update (map (lambda x: x.seq, self.edges_start[cur[0:-1]]))
        # Forward from first
        stack = set ([first])
        while (stack):
            cur = stack.pop ()
            if self.nodes[cur].is_marked != 1: continue
            self.nodes[cur].is_marked = 2
            stack.update (map (lambda x: x.seq, self.edges_end[cur[1:]]))
        
